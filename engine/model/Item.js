const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  url: String,
  name: String,
  module: String,
  created: Number,
  updated: Number,
  tracking: {
    on: Boolean,
    refreshInterval: Number,
    lastRefreshTime: Number,
  },
  details: {
    name: String,
  },
  watches: [{
    price: Number,
    email: String,
  }],
  data: {
    current: {
      price: Number,
      availability: String, // 'unknown' / 'no' / 'low' / 'yes'
    },
    prices: [{
      date: Number,
      price: Number,
    }],
    availabilities: [{
      date: Number,
      available: String, // 'unknown' / 'no' / 'low' / 'yes'
    }],
  },
});

itemSchema.statics.createDefault = function(params = {}) {
  return new Item({
    url: params.url || '',
    name: params.name || '',
    module: undefined,
    created: Date.now(),
    updated: undefined,
    tracking: {
      on: true,
      refreshInterval: 10,
      lastRefreshTime: undefined,
    },
    details: {
      name: '',
    },
    watches: [],
    data: {
      current: {
        price: undefined,
        availability: undefined,
      },
      prices: [],
      availabilities: [],
    },
  });
};

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;
