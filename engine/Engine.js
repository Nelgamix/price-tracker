const Amazon = require('./module/Amazon');
const InstantGaming = require('./module/InstantGaming');

const modules = {
  Amazon: new Amazon(),
  InstantGaming: new InstantGaming(),
};

class Engine {
  constructor() {
    this.items = [];
  }

  setItems(items) {
    this.items = items;

    // Add tracking
    for (const item of items) {
      this.addRefreshInterval(item);
    }

    return this.refresh();
  }

  addItem(item) {
    this.items.push(item);

    // Add tracking
    this.addRefreshInterval(item);

    return this.refresh(item);
  }

  async refresh(item) {
    let items = item ? [item] : this.items;

    for (const item of items) {
      if (!item.module) {
        // Search for module
        item.module = this.findModule(item);

        if (!item.module) {
          console.error('No module for item', item);
          continue;
        }
      }

      try {
        await this.refreshItem(item, modules[item.module]);
      } catch (e) {
        console.error('Error when refreshing item', e);
      }
    }
  }

  findModule(item) {
    for (const key in modules) {
      if (modules[key].accepts(item)) {
        return key;
      }
    }
  }

  async refreshItem(item, module) {
    const start = Date.now();
    await module.refresh(item);
    const refreshTime = Date.now() - start;

    console.log('Refreshed ' + item.name + ' in ' + refreshTime + ' ms');

    item.tracking.lastRefreshTime = refreshTime;
    item.updated = Date.now();

    await item.save();
  }

  addRefreshInterval(item) {
    if (item.tracking.on) {
      const refreshInterval = item.tracking.refreshInterval * 60000;
      console.log('Setting refresh interval for ' + item.name + ' every ' + item.tracking.refreshInterval + ' minutes');
      setInterval(() => this.refresh(item), refreshInterval);
    }
  }
}

module.exports = new Engine();
