const Module = require('./Module');

const Axios = require('axios');
const cheerio = require('cheerio');

class Amazon extends Module {
  accepts(item) {
    return /https:\/\/www\.amazon\.fr\/dp\/.*/.test(item.url);
  }

  async refresh(item) {
    const res = await Axios.get(item.url);

    const $ = cheerio.load(res.data);
    let element;

    // Fetch price
    element = $('#price_inside_buybox');
    const priceVal = element.html().trim();
    const price = Number(/([0-9]{1,},[0-9]{2})/g.exec(priceVal)[0].replace(',', '.'));

    // Fetch details
    if (!item.details.name) {
      element = $('#productTitle').html().trim();
      item.details.name = element;
    }

    this.addEntry(item, price, 'yes');
  }
}

module.exports = Amazon;
