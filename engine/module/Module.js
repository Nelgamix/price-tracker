class Module {
    addEntry(item, price, availability) {
        if (item.data.current.price !== price) {
            // Update price
            item.data.prices.push({
                date: Date.now(),
                price,
            });

            item.data.current.price = price;
        }

        if (item.data.current.availability !== availability) {
            // Update price
            item.data.availabilities.push({
                date: Date.now(),
                availability,
            });

            item.data.current.availability = availability;
        }
    }
}

module.exports = Module;
