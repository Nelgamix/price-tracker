const Module = require('./Module');

const Axios = require('axios');
const cheerio = require('cheerio');

class InstantGaming extends Module {
    accepts(item) {
        return /https:\/\/www\.instant-gaming\.com\/fr\/.+/.test(item.url);
    }

    async refresh(item) {
        const res = await Axios.get(item.url);

        const $ = cheerio.load(res.data);
        let element;

        // Fetch price
        element = $('.price');
        const priceVal = element.html().trim();
        const price = Number(/([0-9]{1,}\.[0-9]{2})/g.exec(priceVal)[0]);

        // Fetch details
        if (!item.details.name) {
            element = $('.title h1').html().trim();
            item.details.name = element;
        }

        this.addEntry(item, price, 'yes');
    }
}

module.exports = InstantGaming;
