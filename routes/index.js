const express = require('express');
const Item = require('../engine/model/Item.js');
const Engine = require('../engine/Engine');

const router = express.Router();

router.get('/', async function(req, res) {
  const items = await Item.find();
  res.render('index', {items});
});

router.get('/add-item', async function(req, res) {
  res.render('add-item');
});

router.get('/item/:id', async function(req, res) {
  const item = await Item.findById(req.params.id);
  res.render('item', {item});
});

router.post('/', async function(req, res) {
  const url = req.body.url;
  const name = req.body.name;
  const desiredPrice = req.body.desiredPrice;
  const email = req.body.email;

  if (!url) {
    return;
  }

  const newItem = Item.createDefault({
    url,
    name,
  });

  if (desiredPrice && email) {
    newItem.watches.push({
      price: desiredPrice,
      email,
    });
  }

  await Engine.addItem(newItem);

  res.redirect('/');
});

module.exports = router;
